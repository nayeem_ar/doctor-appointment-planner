import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  MatDialog,
  MatDialogConfig,
  MatDialogRef,
} from '@angular/material/dialog';
import { CreateAppointmentComponent } from './create-appointment/create-appointment.component';
import { AppointmentData } from '../models/appointment-data.model';
import { DetailsViewComponent } from '../shared/details-view/details-view.component';

@Component({
  selector: 'app-appointment',
  templateUrl: './appointment.component.html',
  styleUrls: ['./appointment.component.scss'],
})
export class AppointmentComponent implements OnInit {
  items: any[] = [];
  appointmentData: AppointmentData[] = [];
  currentYear: number = new Date().getFullYear();
  currentMonth: number = new Date().getMonth() + 1;
  selectedValue!: any;
  filteredData: AppointmentData[] = [];
  allData: AppointmentData[] = [];
  months = [
    { name: 'January', id: '1' },
    { name: 'February', id: '2' },
    { name: 'March', id: '3' },
    { name: 'April', id: '4' },
    { name: 'May', id: '5' },
    { name: 'June', id: '6' },
    { name: 'July', id: '7' },
    { name: 'August', id: '8' },
    { name: 'September', id: '9' },
    { name: 'October', id: '10' },
    { name: 'November', id: '11' },
    { name: 'December', id: '12' },
  ];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.createMonthsArray();
    this.selectedValue = this.route.snapshot.paramMap.get('id');

    if (!isNaN(+this.selectedValue) && (+this.selectedValue > 0 && +this.selectedValue < 13)) {
      this.getData(this.selectedValue);
    } else {
      this.selectedValue = this.currentMonth.toString();
      this.router.navigate(['month/' + this.currentMonth]);
      this.getData(this.currentMonth);
    }
  }

  getData(monthId: number): void {
    this.allData = [];
    this.filteredData = [];
    this.allData = JSON.parse(localStorage.getItem('appointmentData') || '[]');
    this.filteredData = this.allData.filter((item: AppointmentData) => {
      return new Date(item.date).getMonth() + 1 === +monthId;
    });
    this.items = [];
    this.createMonthsArray();
    if (this.filteredData.length) {
      this.items.forEach((item) => {
        this.filteredData.forEach((value) => {
          if (new Date(value.date).getDate() === item.month) {
            item.data.push(value);
          }
        });
      });
    } else {
      this.items = [];
      this.createMonthsArray();
    }
  }

  onAppointment(item: AppointmentData) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '600px';
    dialogConfig.data = { appointmentData: item };
    this.dialog.open(DetailsViewComponent, dialogConfig);
  }

  createMonthsArray(): void {
    const numberOfDays = new Date(this.currentYear, this.selectedValue, 0).getDate();;
    for (let i = 1; i <= numberOfDays; i++) {
      this.items.push({ month: i, data: [] });
    }
  }

  onMonth(id: string): void {
    this.items = [];
    this.createMonthsArray();
    this.router.navigate(['month/' + id]);
    this.getData(+id);
  }

  createNewAppointment(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '600px';
    const dialogRef = this.dialog.open(
      CreateAppointmentComponent,
      dialogConfig
    );

    dialogRef.afterClosed().subscribe((data: AppointmentData) => {
      if (data) {
        let getItems: AppointmentData[] = JSON.parse(
          localStorage.getItem('appointmentData') || '[]'
        );
        getItems.push(data);
        localStorage.setItem('appointmentData', JSON.stringify(getItems));
        this.getData(this.selectedValue);
      }
    });
  }
}
