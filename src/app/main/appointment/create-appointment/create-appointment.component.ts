import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-create-appointment',
  templateUrl: './create-appointment.component.html',
  styleUrls: ['./create-appointment.component.scss'],
})
export class CreateAppointmentComponent implements OnInit {
  form!: FormGroup;
  genders = ['Male', 'Female', 'Others']

  constructor(
    private _fb: FormBuilder,
    private _dialogRef: MatDialogRef<CreateAppointmentComponent>
  ) {
    _dialogRef.disableClose = true;
  }

  ngOnInit(): void {
    this.form = this._fb.group({
      firstName: ['', [Validators.required, Validators.maxLength(40)]],
      lastName: ['', [Validators.required, Validators.maxLength(40)]],
      email: ['', [Validators.required, Validators.email]],
      gender: [''],
      age: [''],
      date: ['', Validators.required],
      time: ['', Validators.required],
    });
  }

  public checkError = (controlName: string, errorName: string) => {
    return this.form.controls[controlName].hasError(errorName);
  }

  save(): void {
    this._dialogRef.close(this.form.value);
  }

  close() {
    this._dialogRef.close();
  }
}
