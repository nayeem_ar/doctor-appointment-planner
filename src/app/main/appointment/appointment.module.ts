import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';

import { AppointmentComponent } from './appointment.component';
import { RouterModule, Routes } from '@angular/router';
import { CreateAppointmentComponent } from './create-appointment/create-appointment.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DetailsViewModule } from '../shared/details-view/details-view.module';

const currentMonth = new Date().getMonth() + 1;
const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'month/' + currentMonth,
    pathMatch: 'full',
  },
  {
    path: 'month/:id',
    component: AppointmentComponent,
  },
  {
    path: '**',
    redirectTo: 'month/' + currentMonth,
  },
];

@NgModule({
  declarations: [AppointmentComponent, CreateAppointmentComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(appRoutes),
    MatFormFieldModule,
    MatSelectModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    DetailsViewModule,
  ],
})
export class AppointmentModule {
  currentMonth: number = new Date().getMonth() + 1;
}
