export interface AppointmentData {
  firstName: string,
  lastName: string,
  age: number,
  email: string,
  gender: string,
  date: string,
  time: string
}
