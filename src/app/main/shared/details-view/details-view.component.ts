import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppointmentData } from 'src/app/main/models/appointment-data.model';


@Component({
  selector: 'app-details-view',
  templateUrl: './details-view.component.html',
  styleUrls: ['./details-view.component.scss'],
})
export class DetailsViewComponent implements OnInit {
  appointmentData!: AppointmentData;
  constructor(
    private _dialogRef: MatDialogRef<DetailsViewComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {
    this.appointmentData = this.data.appointmentData;
    console.log(this.appointmentData);

  }

  onClose(): void {
    this._dialogRef.close();
  }
}
