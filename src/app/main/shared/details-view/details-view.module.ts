import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { DetailsViewComponent } from './details-view.component';

@NgModule({
  declarations: [DetailsViewComponent],
  imports: [CommonModule, MatDividerModule, MatListModule, MatIconModule],
})
export class DetailsViewModule {}
